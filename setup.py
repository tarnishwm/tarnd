import setuptools

setuptools.setup(
    name="tarnd",
    version="0.0.1",
    author="Kalyan Sriram",
    description="A Python daemon to drive the tarnish Wayland compositor",
    license="GPL",
    keywords="daemon tarnishwm wayland",
    packages=setuptools.find_packages(),
    scripts=["bin/tarnd"]
)
