import daemon
import zmq
import struct
from enum import Enum
from tarnd.repcodes import RepCodes
from tarnd.reqcodes import ReqCodes
from tarnd.comms import CommunicationsManager
from tarnd.modifiers import Modifiers

def main():
    comms = CommunicationsManager()
    comms.bind(comms.rep_socket, "tcp://*:5555")
    comms.connect(comms.req_socket, "tcp://localhost:5556")

    while True:
        reqcode, reqval1, reqval2 = comms.rep_recv()
        comms.rep_send(RepCodes.TARN_REP_OK, 1)
        print(reqcode)

        if reqcode == ReqCodes.TARN_REQ_KEY_RELEASED:
            if reqval1 == 36 and reqval2 == Modifiers.WLR_MODIFIER_ALT:
                result, pid = comms.req_send_launch("termite")
                print("Program with PID: {} returned status {}".format(pid, result))

        # message = rep_socket.recv()
        # # print("Received request: %s" % message)
        # (reqcode, reqval) = struct.unpack("II", message)
        # print(reqcode)
        # # if reqcode == RepCodes.TARN_REP_OK:
        #     # print("Received ok")
        # # elif reqcode == RepCodes.TARN_REP_ERROR:
        #     # print("Received error")
        # # else:
        #     # print("Unable to parse")
        # # print("Status: {}", status)
        # repcode = RepCodes.TARN_REP_OK
        # repval = 1
        # rep_socket.send(struct.pack("II", repcode, repval))
        # if reqcode == ReqCodes.TARN_REQ_KEY_RELEASED:
        #     # print("Key released: {}".format(reqval))
        #     if reqval == 36:
        #         repval = 1
        #         cmd = bytes("termite", "utf-8")
        #         # print(cmd)
        #         cmd_req = struct.pack("II", ReqCodes.TARN_REQ_LAUNCHCMD,
        #                 len(cmd)) + cmd
        #         # print(cmd_req)
        #         req_socket.send(cmd_req)
        #         launch_res = req_socket.recv()
        #         (result, pid) = struct.unpack("II", launch_res)
        #         print("Launch PID: {}", pid)

if __name__ == "__main__":
    main()
