import zmq
import struct
import logging
from tarnd.repcodes import RepCodes
from tarnd.reqcodes import ReqCodes

class CommunicationsManager:
    def __init__(self):
        """
        Create a CommunicationsManager object to
        communicate with the compositor.

        This CommunicationsManager holds both a
        REPLY stream (which is for receiving data such
        as keypresses from the compositor and for
        sending replies such as TARN_REP_OK) and a REQUEST
        stream (for sending messages to the compositor and
        getting back a reply such as an exit code).
        """
        self.context = zmq.Context()
        self.rep_socket = self.context.socket(zmq.REP)
        self.req_socket = self.context.socket(zmq.REQ)

        # self.rep_socket.bind("tcp://*:5555")
        # self.req_socket.connect("tcp://127.0.0.1:5556")

    def bind(self, socket, address):
        socket.bind(address)

    def connect(self, socket, address):
        socket.connect(address)

    def rep_recv(self):
        """
        Receive and unpack a data struct from the reply stream.

        Returns reply code and value.
        """
        message = self.rep_socket.recv()
        reqcode, reqval1, reqval2 = struct.unpack("III", message)

        # Do some nice error checking
        if reqcode == RepCodes.TARN_REP_OK:
            logging.info("Received OK, Code: 0")
        elif reqcode == RepCodes.TARN_REP_ERROR:
            logging.error("Received Error, Code: 1")
        elif reqcode == ReqCodes.TARN_REQ_KEY_PRESSED:
            logging.info("Key Pressed, Code: 10")
        elif reqcode == ReqCodes.TARN_REQ_KEY_RELEASED:
            logging.info("Key Released, Code: 20")
        else:
            logging.warning("Received code {}, not recognized".format(reqcode))

        return reqcode, reqval1, reqval2

    def req_recv(self):
        """
        Receive and unpack a data struct from the request stream.

        Returns request code and value.
        """
        message = self.req_socket.recv()
        reqcode, reqval = struct.unpack("II", message)

        return reqcode, reqval

    def send(self, socket, data):
        """
        Send raw data through the specified socket.

        This function is rarely used directly; for
        higher level functions, check out rep_send
        and req_send.
        """
        socket.send(data)

    def rep_send(self, code, val):
        """
        Pack a reply code and value into a struct
        and send it to the reply stream.

        This is generally used for confirming that the received reply data
        was OK.

        TODO: Error handling
        """
        payload = struct.pack("II", code, val)
        self.rep_socket.send(payload)

    def req_send(self, code, value):
        """
        Pack a request code and value into a struct
        and send it to the request stream.

        NOTE: If you are trying to send a LAUNCHCMD
        to the Tarnish compositor, use req_send_launch().

        TODO: Error handling
        """
        payload = struct.pack("II", code, val)
        self.rep_socket.send(payload)

    def req_send_launch(self, command):
        """
        Convenience function for sending TARN_REQ_LAUNCHCMDs.

        Since the method for sending LAUNCHCMD is different
        from the standard pack-into-a-struct-and-send-it-along
        method, this is a separate function from req_send.

        If you are looking for the method to send a struct
        with a req_code and value, use req_send.

        Returns command result and PID.

        TODO: Maybe standardize LAUNCHCMD and create a slightly less
        "hacky" way to send them?
        """
        payload = struct.pack("II", ReqCodes.TARN_REQ_LAUNCHCMD, len(command))
        payload += bytes(command, "utf-8") # IDK if this is slower, but it sure looks nicer

        self.req_socket.send(payload)

        # Get status of command (did it work?)
        result, pid = self.req_recv()

        return result, pid
